#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include <ciszek_mateusz.h>
#include "bogusz_krzysztof.h"
#include <menu.h>


using namespace std;


int main()
{

    int liczba_pizz=0, liczba_napoi=0, liczba_promocji=0;

    load_numbers(liczba_pizz, liczba_napoi, liczba_promocji);

    vector<string>nazwy_pizz(liczba_pizz);
    vector<string>skladniki_pizz(liczba_pizz);
    vector<float>ceny_pizz(liczba_pizz*3);        //mala,srednia,duza,mala,srednia,duza, co 3 komorki zmiana rodzaju pizzy

    vector<string>nazwy_napoi(liczba_napoi);
    vector<float>ceny_napoi(liczba_napoi*3);

    vector<string>nazwy_promocji(liczba_promocji);
    vector<string>opis_promocji(liczba_promocji);

//cout << "test";

    load_pizzas(liczba_pizz, nazwy_pizz, skladniki_pizz, ceny_pizz);
    load_drinks(liczba_napoi, nazwy_napoi, ceny_napoi);
    load_promotions(liczba_promocji, nazwy_promocji, opis_promocji);


    menu_g(nazwy_pizz, skladniki_pizz, ceny_pizz, nazwy_napoi, ceny_napoi, nazwy_promocji, opis_promocji);



    return 0;
}

