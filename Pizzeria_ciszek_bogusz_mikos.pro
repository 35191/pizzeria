TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        bogusz_krzysztof.cpp \
        ciszek_mateusz.cpp \
        main.cpp \
        menu.cpp

HEADERS += \
    bogusz_krzysztof.h \
    ciszek_mateusz.h \
    menu.h
