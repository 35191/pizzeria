#include <ciszek_mateusz.h>
#include "bogusz_krzysztof.h"
#include <iostream>
#include <fstream>
#include <string>

void load_numbers(int &liczba_pizz, int &liczba_napoi,int &liczba_promocji)
{
    string pusty;

    fstream file_elements;

    file_elements.open( "documents_txt/liczba elementow.txt" );

    if( file_elements.good() != true )
    {
        cout << "Nie udalo sie odczytac pliku z liczbami pizz, napoi i promocji";
        file_elements.close();
    }
    else
    {
        getline(file_elements, pusty);
        file_elements >> liczba_pizz;
        getline(file_elements, pusty);
        getline(file_elements, pusty);
        file_elements >> liczba_napoi;
        getline(file_elements, pusty);
        getline(file_elements, pusty);
        file_elements >> liczba_promocji;
//kod testowy

//        cout<<liczba_pizz<<endl;
//        cout<<liczba_napoi<<endl;
//        cout<<liczba_promocji<<endl<<endl;

//        file_elements.close();
    }
}

void load_pizzas(int liczba_pizz, vector<string> &nazwy_pizz, vector<string> &skladniki_pizz, vector<float> &ceny_pizz)
{
    int n = 0;
    string pusty;

    fstream file_pizza;

    file_pizza.open( "documents_txt/pizza.txt" );

    if( file_pizza.good() != true )
    {
        cout << "Nie udalo sie odczytac pliku z pizzami";
        file_pizza.close();
    }
    else
    {
        for(int i=0; i<liczba_pizz; i++)
        {
            file_pizza >> nazwy_pizz[i];

            getline(file_pizza, pusty);
            getline(file_pizza, skladniki_pizz[i]);

            for(int j=n; j<n+3; j++)
            {
                file_pizza >> ceny_pizz[j];
            }

            n += 3;
        }

        file_pizza.close();
    }

}

void load_drinks(int liczba_napoi, vector<string> &nazwy_napoi, vector<float> &ceny_napoi)
{
    int n = 0;

    fstream file_drinks;

    file_drinks.open("documents_txt/drink.txt");
    if( file_drinks.good() != true )
    {
        cout<<"Nie udalo sie odczytac pliku z napojami";
        file_drinks.close();
    }
    else
    {
        for(int i=0; i<liczba_napoi; i++)
        {
            file_drinks >> nazwy_napoi[i];
            for(int j=n; j<n+3; j++)
            {
                file_drinks >> ceny_napoi[j];
            }

            n += 3;
        }

        file_drinks.close();
    }
}

void load_promotions(int liczba_promocji, vector<string> &nazwy_promocji, vector<string> &opis_promocji)
{
    fstream file_promocje;

    file_promocje.open( "documents_txt/promocje.txt" );

    if( file_promocje.good() != true )
    {
        cout << "Nie udalo sie odczytac pliku z promocjami";
        file_promocje.close();
    }
    else
    {
        for(int i=0; i<liczba_promocji; i++)
        {
            getline(file_promocje, nazwy_promocji[i]);
            getline(file_promocje, opis_promocji[i]);
        }
//kod testowy
/*
        for(int i=0; i<liczba_promocji; i++)
        {
            cout << nazwy_promocji[i]<<endl;
            cout<<opis_promocji[i]<<endl;
        }
*/
        file_promocje.close();
    }
}
