#ifndef BOGUSZ_KRZYSZTOF_H
#define BOGUSZ_KRZYSZTOF_H

#include <string>
#include <vector>

#endif // BOGUSZ_KRZYSZTOF_H

using namespace std;

void load_numbers(int &liczba_pizz, int &liczba_napoi, int &liczba_promocji);
void load_pizzas(int liczba_pizz, vector<string> &nazwy_pizz, vector<string> &skladniki_pizz, vector<float> &ceny_pizz);
void load_drinks(int liczba_napoi, vector<string> &nazwy_napoi, vector<float> &ceny_napoi);
void load_promotions(int liczba_promocji, vector<string> &nazwy_promocji, vector<string> &opis_promcji);
