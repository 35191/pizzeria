#ifndef MENU_H
#define MENU_H



#endif // MENU_H


#include <vector>
#include <iostream>

using namespace std;

void menu_g(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
void menu_p(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
void menu_n(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
void menu_promo(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
void menu_print_order(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
void menu_delete_item(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
void menu_info_promo(vector<string>&nazwy_pizz, vector<string>&skladniki_pizz, vector<float>&ceny_pizz, vector<string>&nazwy_napoi, vector<float>&ceny_napoi, vector<string>&nazwy_promocji, vector<string>&opis_promocji );
